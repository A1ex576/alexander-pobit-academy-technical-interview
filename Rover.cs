using System;
using System.Collections.Generic;
using System.IO;

public class Rover
{
    private class Node
    {
        public int Distance { get; set; }
        public Node LastNode { get; set; }

        private int[] _coords;
        private int[] _cost = new int[8];

        private int[] _diagonalCosts = new int[8];

        private Node[] _neigbors = new Node[8];

        public Node()
        {
            Distance = int.MaxValue;
            for (int i = 0; i < _diagonalCosts.Length; i++)
            {
                _diagonalCosts[i] = 1;
            }
        }

        public void SetCoords(int[] coords)
        {
            _coords = coords;
        }

        public override string ToString()
        {
            return $"[{_coords[0]}][{_coords[1]}]";
        }

        public void AddNeigbor(int index, Node node)
        {
            _neigbors[index] = node;
        }

        public Node getNeigbor(int i)
        {
            return _neigbors[i];
        }

        public int getDIagonalCost(int index)
        {
            return _diagonalCosts[index];
        }

        public void IncreassDiagonalCost(int index)
        {
            _diagonalCosts[index]++;
        }

        public void SetAllDiagonalsCosts(int cost)
        {
            for (int i = 0; i < _diagonalCosts.Length; i++)
            {
                _diagonalCosts[i] = cost;
            }
        }

        public void setCost(int index, int cost)
        {
            _cost[index] = cost;
        }

        public int getCost(int index)
        {
            return _cost[index];
        }
    }

    private static List<Node> _nodes = new List<Node>();

    public static void CalculateRoverPath(string[,] map)
    {
        if (!CheckMap(map))
        {
            return;
        }

        GenerateNodes(map);
        List<Node> listNodes = new List<Node>();
        _nodes[0].Distance = 0;
        listNodes.Add(_nodes[0]);
        int cost = 0;
        int steps = 0;
        int calculatedDistance = 0;
        bool isPathFinded = false;
        while (listNodes.Count > 0)
        {
            Node current = listNodes[0];
            if (current == _nodes[_nodes.Count - 1])
            {
                List<Node> path = new List<Node>();
                steps++;
                calculatedDistance = current.Distance;
                path.Add(current);
                current = current.LastNode;
                path.Add(current);
                while (current != _nodes[0])
                {
                    steps++;
                    current = current.LastNode;
                    path.Add(current);
                }
                isPathFinded = true;
                SaveResult(path, calculatedDistance, steps);
            }
            listNodes.RemoveAt(0);
            for (int i = 0; i < 8; i++)
            {
                Node neigbor = current.getNeigbor(i);
                if (neigbor == null)
                {
                    continue;
                }
                int distance = current.Distance;
                if (i == 0 || i == 2 || i == 4 || i == 6)
                {
                    distance += current.getDIagonalCost(i) + current.getCost(i);
                    current.IncreassDiagonalCost(i);
                }
                else
                {
                    distance += 1 + current.getCost(i);
                }
                if (neigbor.Distance == int.MaxValue)
                {
                    neigbor.Distance = distance;
                    neigbor.LastNode = current;
                    neigbor.SetAllDiagonalsCosts(current.getDIagonalCost(i));
                    cost = distance;
                    listNodes.Add(neigbor);
                }
                else if (distance < neigbor.Distance)
                {
                    neigbor.LastNode = current;
                    neigbor.Distance = distance;
                    neigbor.SetAllDiagonalsCosts(current.getDIagonalCost(i));
                    cost = distance;
                }
                listNodes.Sort((x, y) => x.Distance.CompareTo(y.Distance));
            }
        }
        if (!isPathFinded)
        {
            SaveResult("There are no path");
        }
    }

    private static bool CheckMap(string[,] map)
    {
        if (map.Length == 0)
        {
            SaveError("Cannor start movement because map is empty");
            throw new Exception("CannotStartMovement");
        }
        int rows = map.GetUpperBound(0) + 1;
        int columns = map.Length / rows;
        if (rows == 1 && columns == 1)
        {
            SaveResult("[0][0]" + Environment.NewLine + "steps: 0" + Environment.NewLine + "fuel: 0");
            return false;
        }
        int res = 0;
        if (map[0, 0] == "X")
        {
            SaveError("Cannor start movement because start point have \"X\"");
            throw new Exception("CannotStartMovement");
        }
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                if (map[i, j] == "X")
                {
                    continue;
                }
                if (int.TryParse(map[i, j], out res))
                {
                    continue;
                }
                SaveError("Cannor start movement because map contain weird data");
                throw new Exception("CannotStartMovement");
            }
        }
        return true;
    }

    private static void SaveError(string message)
    {
        try
        {
            using (StreamWriter stream = new StreamWriter(Path.Combine(Environment.CurrentDirectory, "path-plan.txt"), false, System.Text.Encoding.Default))
            {
                stream.WriteLine(message);
            }
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    private static void SaveResult(string comments)
    {
        try
        {
            using (StreamWriter stream = new StreamWriter(Path.Combine(Environment.CurrentDirectory, "path-plan.txt"), false, System.Text.Encoding.Default))
            {
                stream.WriteLine(comments);
            }
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    private static void SaveResult(List<Node> path, int cost, int steps)
    {
        path.Reverse();
        string pathText = "";
        foreach (var coords in path)
        {
            pathText += coords.ToString() + "->";
        }
        pathText = pathText.Remove(pathText.Length - 2, 2);
        pathText += Environment.NewLine + $"steps: {steps}" + Environment.NewLine + $"fuel: {cost}";
        try
        {
            using (StreamWriter stream = new StreamWriter(Path.Combine(Environment.CurrentDirectory, "path-plan.txt"), false, System.Text.Encoding.Default))
            {
                stream.WriteLine(pathText);
            }
        }
        catch (Exception e)
        {
            throw e;
        }
    }

    private static Node[] getPossibleWays(int[] current, int rows, int columns, List<Node> nodes, int counter, out int[][] possibleWays)
    {
        possibleWays = new int[8][];
        Node[] result = new Node[8];
        if (current[0] + 1 < rows)
        {
            result[5] = nodes[counter + columns];
            possibleWays[5] = (new[] { current[0] + 1, current[1] });
        }
        if (current[0] - 1 >= 0)
        {
            result[1] = nodes[counter - columns];
            possibleWays[1] = (new[] { current[0] - 1, current[1] });
        }
        if (current[1] + 1 < columns)
        {
            result[3] = nodes[counter + 1];
            possibleWays[3] = (new[] { current[0], current[1] + 1 });
        }
        if (current[1] - 1 >= 0)
        {
            result[7] = nodes[counter - 1];
            possibleWays[7] = (new[] { current[0], current[1] - 1 });
        }
        if (current[0] - 1 >= 0 && current[1] - 1 >= 0)
        {
            result[0] = nodes[counter - columns - 1];
            possibleWays[0] = (new[] { current[0] - 1, current[1] - 1 });
        }
        if (current[0] - 1 >= 0 && current[1] + 1 < columns)
        {
            result[2] = nodes[counter - columns + 1];
            possibleWays[2] = (new[] { current[0] - 1, current[1] + 1 });
        }
        if (current[0] + 1 < rows && current[1] + 1 < columns)
        {
            result[4] = nodes[counter + columns + 1];
            possibleWays[4] = (new[] { current[0] + 1, current[1] + 1 });
        }
        if (current[0] + 1 < rows && current[1] - 1 >= 0)
        {
            result[6] = nodes[counter + columns - 1];
            possibleWays[6] = (new[] { current[0] + 1, current[1] - 1 });
        }
        return result;
    }

    private static void GenerateNodes(string[,] map)
    {
        int rows = map.GetUpperBound(0) + 1;
        int columns = map.Length / rows;
        int cost = 0;
        int[][] ways;
        int index = 0;
        for (int i = 0; i < map.Length; i++)
        {
            _nodes.Add(new Node());
        }
        int counter = 0;
        for (int i = 0; i < rows; i++)
        {
            for (int j = 0; j < columns; j++)
            {
                int[] current = new int[] { i, j };
                _nodes[counter].SetCoords(current);
                foreach (var nextNode in getPossibleWays(current, rows, columns, _nodes, counter, out ways))
                {
                    if (nextNode == null)
                    {
                        index++;
                        continue;
                    }
                    if (map[current[0], current[1]] != "X" && map[ways[index][0], ways[index][1]] != "X")
                    {
                        cost = Math.Abs(Convert.ToInt32(map[current[0], current[1]]) - Convert.ToInt32(map[ways[index][0], ways[index][1]]));
                        _nodes[counter].AddNeigbor(index, nextNode);
                        _nodes[counter].setCost(index, cost);
                    }
                    index++;
                }
                index = 0;
                counter++;
            }
        }
    }
}